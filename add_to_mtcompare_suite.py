import json
import configparser
from csv import DictReader
from pathlib import Path
from os import getcwd
import requests
import argparse
from icecream import ic

url_get = 'http://mtcompare.reverso.net/tsapi/GetSources/{}'
url_add = 'http://mtcompare.reverso.net/tsapi/AddTest/{}'
url_delete = 'http://mtcompare.reverso.net/tsapi/DeleteTest/{}'
login = 'http://mtcompare.reverso.net/tsapi/Login?username={}&password={}'


def find_config():
    cwd = Path(getcwd())
    config_path = cwd / 'config.ini'
    if config_path.exists():
        return config_path


def clear_test(test_id: str, usrname: str, pwd: str) -> None:
    with requests.Session() as session:
        y = session.post(login.format(usrname, pwd), json=dict())
        if y.status_code == 200:
            print('OK')
            # get current version
            x = session.get(url_get.format(test_id))
            cases = x.json()
            # but save the test info in case
            with open('../../spellcheck/ginger_test.json', 'w') as out:
                json.dump(cases, out)
            # delete everything
            for idx in [x['Id'] for x in cases]:
                resp = requests.Response()
                while resp.status_code != 200:
                    resp = session.post(url_delete.format(idx))


def add_new(test_id: str, sources: str, source_col: str, target_col: str, category: str) -> None:
    # clear_test(test_id)
    config = configparser.ConfigParser()
    config.read(find_config())
    mtcompare = config['MTCompare']
    
    username = mtcompare['username']
    password = mtcompare['password']

    assert sources.endswith('.csv')
    with requests.Session() as session, open(sources, encoding='utf-8') as inp:
        reader = DictReader(inp)
        y = session.post(login.format(username, password), json=dict())
        if y.status_code == 200:
            print('OK')
            if len(reader.fieldnames) == 1:
                for e, row in enumerate(reader):
                    source = row[source_col].strip()
                    body = {"Id": e+1,
                            "Category": category,
                            "Source": source,
                            "Comments": None,
                            "Score": 1,
                            "References": [
                                    {"Reference": source,
                                    "Score": 1,
                                    "Comments": None}
                                ],
                            }
                    x = session.post(url_add.format(test_id), json=body)
                    if x.status_code != 200:
                        input(body)
            else:
                for e, row in enumerate(reader):
                    incorr = row[source_col].strip()
                    corr = row[target_col].strip()

                    if corr != incorr:
                        body = {"Id": e+1,
                                "Category": category,
                                "Source": incorr,
                                "Comments": None,
                                "Score": 0,
                                "References": [
                                    {"Reference": corr,
                                    "Score": 1,
                                    "Comments": None},
                                    {"Reference": incorr,
                                    "Score": 0,
                                    "Comments": None}
                                ],
                                }
                    else:
                        body = {"Id": e + 1,
                                "Category": category,
                                "Source": incorr,
                                "Comments": None,
                                "Score": 0,
                                "References": [
                                    {"Reference": corr,
                                    "Score": 1,
                                    "Comments": None},
                                ],
                                }
                    x = session.post(url_add.format(test_id), json=body)
                    if x.status_code != 200:
                        input(body)

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--test_id", type=str, help='Test suite ID in MTCompare')
    parser.add_argument("--path", type=str, help="Local path of test to upload")
    parser.add_argument("--source", type=str, help="Source column", default="Original sentence")
    parser.add_argument("--target", type=str, help="Target column", default="Expected sentence")
    parser.add_argument("--category", type=str, help="Category of errors / None if no specific targets", default="None")

    args = parser.parse_args()
    add_new(args.test_id, args.path, args.source, args.target, args.category)