import argparse
import configparser
import re
from tqdm import tqdm
import time
from pathlib import Path
from os import getcwd
from http import HTTPStatus
import importlib
from nltk import download
from nltk.tokenize.treebank import TreebankWordDetokenizer
from nltk.tokenize import word_tokenize

from nmtcoreutils.api.mtcompare.service import MtCompareService
from nmtcoreutils.api.mtcompare.evaluation import prepare_mt_compare_source, SourceEvaluationItem, SourceEvaluation

download('punkt')

def detokenize(s):
    tokens = s.split()
    return TreebankWordDetokenizer().detokenize(tokens)


def tokenize(s):
    tokens = word_tokenize(s)
    return ' '.join(tokens)


REPLACE_DICT = {
    '’': '\'',
    '‘': '\'',
    '``': '"',
    "''": '"',
    "“": '"',
    "”": '"'
}
opening_single_quote = re.compile(r"^'[^\s]")


def replace(s):
    for key in REPLACE_DICT:
        s = s.replace(key, REPLACE_DICT[key])
    return s


def remove_extra_space(s):
    pattern = r'\" (.+?) \"'
    target_indices = set()
    for m in re.finditer(pattern, s):
        target_indices.add(m.start() + 1)
        target_indices.add(m.end() - 2)
    res = []
    for i in range(len(s)):
        if i not in target_indices:
            res.append(s[i])
    return ''.join(res)


def post_process(s):
    s = detokenize(s)
    s = remove_extra_space(s)
    return s


def post_process_all(predictions):
    return [post_process(s) for s in predictions]


def find_config():
    cwd = Path(getcwd())
    config_path = cwd / 'config.ini'
    if config_path.exists():
        return config_path


def main(args):
    config = configparser.ConfigParser()
    config.read(find_config())

    test_id = args.test_id
    mtcompare = config['MTCompare']

    host = mtcompare['host']
    port = mtcompare['port']
    username = mtcompare['username']
    password = mtcompare['password']

    service = MtCompareService(host, port, with_session=True)
    _, code = service.login(username=username, password=password).result()
    if code == HTTPStatus.OK:
        print('Login success!')
        res, _ = service.get_sources(test_id).result()

        source_ids = []
        sources = []
        versions = []

        print("Retrieving data from MTCompare...")

        for i in tqdm(range(len(res))):
            test_suite = res[i]

            source_id = test_suite['Id']
            source_ids.append(source_id)

            source = test_suite['Source']
            source = prepare_mt_compare_source(source)
            source = replace(source)
            source = tokenize(source)
            source = replace(source)
            sources.append(source)

            version = test_suite['Version']
            versions.append(version)

        print("Data retrieval complete.")

        print("Predicting...")

        model_predict_for_mtcompare = importlib.import_module(args.model, package=None).predict_for_mtcompare
        predictions = model_predict_for_mtcompare(sources)

        print("Prediction complete.")

        assert len(predictions) == len(source_ids), "Number of predictions must equal number of ids!"
        assert len(predictions) == len(versions), "Number of predictions must equal number of versions!"

        # process results and upload to MTCompare
        print("Processing results...")

        source_evaluation_items = []

        for i in range(len(predictions)):
            source_id = source_ids[i]
            version = versions[i]
            prediction = predictions[i]
            source_evaluation_item = SourceEvaluationItem(source_id, version, prediction)
            source_evaluation_items.append(source_evaluation_item)

        print("Processing done")

        print("Uploading to MTCompare...")

        description = "Prediction results for model: {}, time: {}".format(args.name, time.strftime("%Y%m%d-%H%M%S"))
        if args.version != None:
            tag = args.name+':'+args.version
        else:
            tag = args.name
        source_evaluation = SourceEvaluation(test_id)

        for item in source_evaluation_items:
            source_evaluation.add_source_evaluation(item)

        _, _ = service.put_results(source_evaluation,
                                               description=description,
                                               tag=tag).result()

        print("Results uploaded to MTCompare.")

    else:
        print('Login unsuccessful!')


if __name__ == '__main__':
    # read parameters
    parser = argparse.ArgumentParser()
    parser.add_argument("--test_id", type=str, help='Test suite ID in MTCompare')
    parser.add_argument("--model", type=str, help="path of the file predict_for_mtcompare of the model", default="models.Gramformer.predict_for_mtcompare")
    parser.add_argument("--name", type=str, help="Model name")
    parser.add_argument("--version", type=str, help="Model version", default=None)

    args = parser.parse_args()
    main(args)