import language_tool_python

def predict_for_mtcompare(inputs_sent):
    #tool = language_tool_python.LanguageTool('en-US')         # USE LANGUAGETOOL LOCALLY - NOT ALWAYS THE NEWEST VERSION
    tool = language_tool_python.LanguageToolPublicAPI('en-US') # USE LANGUAGETOOL API - LIMITS AND RESTRICTIONS
    predictions = []
    for sent in inputs_sent:
        predictions.append(tool.correct(sent))
    return predictions