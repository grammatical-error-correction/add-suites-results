import spacy
from neuspell import BertChecker

spacy.load("en_core_web_sm")

def predict_for_mtcompare(input_sents):
    checker = BertChecker()
    checker.from_pretrained() # ISSUE WITH NEUSPELL - https://github.com/neuspell/neuspell/issues/36
    predictions = []
    for sent in input_sents:
        predictions.append(checker.correct(sent))
    return predictions