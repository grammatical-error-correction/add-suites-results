from models.Gramformer.model import Model
from models.Gramformer.correction import Correction

def predict_for_mtcompare(input_sents):
    model = Model("T5",  "prithivida/grammar_error_correcter_v1")
    correction = Correction()
    for sent in input_sents:
        correction.correct_sentence(sent, model.happy_tt, model.settings)
    return correction.corrected_sentences