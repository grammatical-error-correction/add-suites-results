from __future__ import print_function
import ProWritingAidSDK
from ProWritingAidSDK.rest import ApiException

configuration = ProWritingAidSDK.Configuration()
configuration.host = 'https://api.prowritingaid.com'
configuration.api_key['licenseCode'] = '9301637B-0DD5-4C9C-9707-FF9233AD99D9' # ENTER YOUR API KEY ~500 SENTENCES FREE

# create an instance of the API class
api_instance = ProWritingAidSDK.TextApi(ProWritingAidSDK.ApiClient('https://api.prowritingaid.com'))

def predict_for_mtcompare(input_sents):
    predictions = []
    for sent in input_sents:  
        try:
            api_request = ProWritingAidSDK.TextAnalysisRequest(sent,
                                                            ["grammar"],
                                                            "General",
                                                            "en")
            api_response = api_instance.post(api_request)
        except ApiException as e:
            print("Exception when calling API: %s\n" % e)
        tags = api_response.result.tags
        correct_sentence = sent
        # Apply all the tags to the original string to get a corrected string 
        # Important to work through the tags backward to that indexes don't change
        for tag in reversed(tags):
            replacement = '' if tag.suggestions[0] == '(omit)' else tag.suggestions[0] 
            correct_sentence = correct_sentence[0:tag.start_pos] + replacement + correct_sentence[tag.end_pos+1:]
        predictions.append(correct_sentence)
    return predictions