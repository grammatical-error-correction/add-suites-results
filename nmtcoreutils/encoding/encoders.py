import os
import subprocess
import sys
from abc import abstractmethod, ABC

from nmtcoreutils import argbuilder
from nmtcoreutils.core import Languages
from nmtcoreutils.versioning import get_max_version, DateVersioning


def get_default_encoder_path(name, src_lang, tgt_lang, version=None):
    encoder_config = os.path.join(
        argbuilder.RELEASES_PATH, argbuilder.RELEASES_MODEL_PATH, name, '{}-{}'.format(src_lang, tgt_lang))
    version = version if version else get_max_version(encoder_config, DateVersioning)
    return os.path.join(encoder_config, version, argbuilder.RELEASES_MODEL_TOKENIZER_PATH)


class Encoder(ABC):

    @abstractmethod
    def encode(self, fname, output_fname, **kwargs):
        pass


class Decoder(ABC):

    @abstractmethod
    def decode(self, fname, output_fname, **kwargs):
        pass


class OoEncoder(Encoder, Decoder):
    DEFAULT_SCRIPT_PATH = os.path.join('/', 'devdata', 'tradooit', 'bin', 'oo')

    def __init__(self, config_path, language: Languages, script_path=None):
        self._script_path = script_path if script_path else OoEncoder.DEFAULT_SCRIPT_PATH
        self._language = language
        self._config_path = config_path

    def encode(self, fname, output_fname, **kwargs):
        command = '{} encode -l {} -i {} -o {} -c {}'.format(
            self._script_path, str(self._language), fname, output_fname, self._config_path)
        subprocess.call(
            command, shell=True, stdout=kwargs.get('stdout', sys.stdout), stderr=kwargs.get('stderr', sys.stderr))

    def decode(self, fname, output_fname, **kwargs):
        command = '{} decode -l {} -i {} -o {} -c {}'.format(
            self._script_path, str(self._language), fname, output_fname, self._config_path)
        subprocess.call(
            command, shell=True, stdout=kwargs.get('stdout', sys.stdout), stderr=kwargs.get('stderr', sys.stderr))
