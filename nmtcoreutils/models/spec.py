import json
from typing import Dict


class Specification:

    def __init__(self, data):
        self._data: Dict = data

    @property
    def data(self) -> dict:
        return self._data

    def __str__(self):
        return json.dumps(self.data)

    def __repr__(self):
        return str(self)


class UniqueSpecification(Specification):

    def __init__(self, data, uri):
        super().__init__(data)
        self.data['uri'] = uri

    @property
    def uuid(self):
        return self.data.get('uri', None)
