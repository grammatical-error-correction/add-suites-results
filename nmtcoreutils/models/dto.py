import json


class ApiDTO(dict):

    def __str__(self):
        return json.dumps(self)

    def __repr__(self):
        return str(self)

    @staticmethod
    def from_data(data: dict):
        return ApiDTO(data)
