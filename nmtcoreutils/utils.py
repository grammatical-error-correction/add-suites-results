import logging
from typing import List

from nmtcoreutils.core import Languages


def setup_logging():
    logging.basicConfig(format='%(asctime)s %(process)d %(levelname)s %(filename)s %(lineno)d: %(message)s',
                        level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')


def group_language_files(languages: List[Languages], fileset):
    existing_files = set(fileset)
    base_names = set(fname[:-len(str(languages[0]))] for fname in existing_files)
    groups = list()

    for base_name in base_names:
        group = list()
        for language in languages:
            fname = '{}{}'.format(base_name, str(language))
            if fname not in existing_files:
                raise RuntimeError('{} is not in the provided files'.format(fname))
            group.append(fname)
        groups.append(tuple(group))
    return groups


def sort_double(data, key1_fx, key2_fx, reverse=False):
    buckets = dict()
    for item in data:
        key = key1_fx(item)
        if key not in buckets:
            buckets[key] = list()
        buckets[key].append(item)

    sorted_list = list()
    for key in sorted(buckets.keys(), reverse=reverse):
        sorted_list += sorted(buckets[key], key=key2_fx, reverse=reverse)
    return sorted_list
