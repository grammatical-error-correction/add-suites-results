import os
import shutil
import subprocess
from abc import ABC, abstractmethod


class Action(ABC):

    @abstractmethod
    def setup(self):
        pass

    @abstractmethod
    def run(self):
        pass

    @abstractmethod
    def cleanup(self):
        pass

    @abstractmethod
    def results(self):
        pass

    def exit(self):
        pass


class RunOnlyAction(Action, ABC):

    def setup(self):
        pass

    def cleanup(self):
        pass

    def results(self):
        pass

    @abstractmethod
    def run(self):
        pass


class FileWriterAction(Action, ABC):

    def __init__(self, save_folder):
        self._save_folder = save_folder
        self._results = list()

    @abstractmethod
    def run(self):
        pass

    @property
    def save_path(self):
        return self._save_folder

    def setup(self):
        os.makedirs(self._save_folder, exist_ok=True)

    def cleanup(self):
        self._results = list()
        try:
            shutil.rmtree(self._save_folder)
        except FileNotFoundError:
            pass

    def results(self):
        return self._results


class BashAction(Action, ABC):

    def __init__(self, command, cwd=None):
        self._command = command
        self._cwd = cwd
        self._output = None

    @abstractmethod
    def setup(self):
        pass

    def run(self):
        if self._cwd:
            self._output = subprocess.call(self._command, cwd=self._cwd, shell=True)
        else:
            self._output = subprocess.call(self._command, shell=True)

    def results(self):
        return self._output

    @abstractmethod
    def cleanup(self):
        pass