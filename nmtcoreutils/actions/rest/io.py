from nmtcoreutils.actions.io import InputSource, OutputSource


class RestfulInputSource(InputSource):
    """
    GET from somewhere
    """
    @property
    def name(self):
        return ''

    def get_next_input(self):
        pass

    @property
    def business(self):
        return 0.0


class RestfulOutputSource(OutputSource):
    """
    POST somewhere
    """
    @property
    def name(self):
        return ''

    def finish(self):
        pass

    def provide_next_output(self, value):
        pass

    @property
    def business(self):
        return 0.0
