from abc import ABC, abstractmethod


class Resource(ABC):

    @property
    @abstractmethod
    def business(self):
        pass

    @property
    @abstractmethod
    def name(self):
        pass


class InputSource(Resource, ABC):

    @abstractmethod
    def get_next_input(self):
        pass


class OutputSource(Resource, ABC):

    @abstractmethod
    def provide_next_output(self, value):
        pass

    @abstractmethod
    def finish(self):
        pass
