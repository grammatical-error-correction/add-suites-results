import logging
import multiprocessing
import os
from multiprocessing import Process

from nmtcoreutils.actions.base_actions import Action


class ActionThread(Action):

    def __init__(self):
        super().__init__()
        self._previous_actions = list()
        self._actions = list()

    @property
    def previous_results(self):
        return self._previous_actions

    def setup(self):
        self._execute_fx(lambda action: action.setup())

    def run(self):
        self._execute_fx(lambda action: action.run())

    def results(self):
        return self._previous_actions[-1].results()

    def cleanup(self):
        self._execute_fx(lambda action: action.cleanup())

    def _execute_fx(self, fx):
        self._previous_actions = list()
        for action_fx in self._actions:
            action = action_fx(self._previous_actions)
            fx(action)
            self._previous_actions.append(action)

    def add_action(self, action_fx):
        self._actions.append(action_fx)


def get_max_number_of_processes():
    return multiprocessing.cpu_count()


class ParallelActionThread(ActionThread):

    def __init__(self, previous_actions, actions=None):
        super().__init__()
        self._actions = list(actions)
        self._results = list()
        self._previous_actions = previous_actions

    def add_action(self, action_fx):
        self._actions.append(action_fx)

    def run(self):
        actions = list(map(lambda fx: fx(self._previous_actions), self._actions))
        processes = [Process(target=thread.run) for thread in actions]

        for thread in processes:
            thread.start()

        for thread in processes:
            thread.join()

        for action in actions:
            results = action.results()
            self._results += results if results else []

    def results(self):
        return self._results


class ParallelAllocationActionThread(ActionThread):

    def __init__(self, gen_fx, input_sources, processes=None):
        super().__init__()
        self._gen_fx = gen_fx
        self._input_sources = input_sources
        self._results = None
        self._actions = self._get_threads(processes=processes if processes else get_max_number_of_processes())

    def _get_threads(self, processes=None):
        processes = processes if processes else get_max_number_of_processes()

        if processes == 1:
            logging.info('1 processes with %s files', len(self._input_sources))
            return [self._gen_fx(self._input_sources)]

        if processes >= len(self._input_sources):
            logging.info('%s processes with 1 file', len(self._input_sources))
            return [self._gen_fx([data_file]) for data_file in self._input_sources]

        file_buckets = [list() for _ in range(processes)]
        size_of_buckets = [0 for _ in range(processes)]
        bucket_pointer = 0
        files = list(self._input_sources)
        file_buckets[bucket_pointer].append(files.pop(0))
        size_of_buckets[bucket_pointer] += os.path.getsize(file_buckets[bucket_pointer][0])
        bucket_pointer += 1

        while len(files) > 0:
            if size_of_buckets[bucket_pointer] >= size_of_buckets[bucket_pointer - 1]:
                # Always pick the biggest
                files = list(reversed(files))
                bucket_pointer += 1
            new_file = files.pop(0)
            file_buckets[bucket_pointer].append(new_file)
            size_of_buckets[bucket_pointer] += os.path.getsize(new_file)
            files = list(sorted(files, key=os.path.getsize))

        file_buckets = [bucket for bucket in file_buckets if len(bucket) > 0]
        logging.info('%s processes each with %s files and bucket sizes of %s',
            len(file_buckets), file_buckets, size_of_buckets[:len(file_buckets)])
        return [self._gen_fx(data_files) for data_files in file_buckets]

    def run(self):
        actions = list(map(lambda fx: fx(None), self._actions))
        processes = [Process(target=thread.run) for thread in actions]

        for thread in processes:
            thread.start()

        for thread in processes:
            thread.join()

    def results(self):
        return self._results
