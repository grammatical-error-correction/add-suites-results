import os
import subprocess

from nmtcoreutils.actions.base_actions import FileWriterAction


class GitSetupAction(FileWriterAction):

    def __init__(self, location, git_repo=None, branch=None):
        super().__init__(os.path.join(location))
        self._git_repo = git_repo
        self._branch = branch

    def setup(self):
        pass

    def run(self):
        if os.path.exists(self.save_path):
            if self._branch:
                subprocess.call('git checkout {}'.format(self._branch), cwd=self.save_path, shell=True)
            subprocess.call('git fetch && git rebase', cwd=self.save_path, shell=True)
        elif self._git_repo is not None:
            subprocess.call('git clone {} {}'.format(self._git_repo, self.save_path),
                            cwd=os.path.split(self.save_path)[0], shell=True)
            if self._branch:
                subprocess.call('git checkout {}'.format(self._branch), cwd=self.save_path, shell=True)
