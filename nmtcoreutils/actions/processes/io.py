import collections

from nmtcoreutils.actions.io import InputSource, OutputSource


class QueueInputSource(InputSource, OutputSource):

    def __init__(self, name, maxlen=None):
        self._name = name
        self._queue = collections.deque() if not maxlen else collections.deque(maxlen=maxlen)

    @property
    def name(self):
        return self._name

    def get_next_input(self):
        return self._queue.popleft()

    def provide_next_output(self, value):
        self._queue.append(value)

    @property
    def business(self):
        return 0.0

    def finish(self):
        pass
