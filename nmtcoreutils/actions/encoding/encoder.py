from nmtcoreutils.actions.base_actions import RunOnlyAction
from nmtcoreutils.core import Languages
from nmtcoreutils.encoding.encoders import OoEncoder
from nmtcoreutils.workspaces.workspaces import File


class EncoderAction(RunOnlyAction):

    def __init__(self, config, language: Languages, input_fname: File, output_fname: File):
        self._config = config
        self._language = language
        self._input_fname = input_fname
        self._output_fname = output_fname

    def run(self):
        encoder = OoEncoder(self._config, self._language)
        encoder.encode(fname=str(self._input_fname), output_fname=str(self._output_fname))


class DecoderAction(RunOnlyAction):

    def __init__(self, config, language: Languages, input_fname: File, output_fname: File):
        self._config = config
        self._language = language
        self._input_fname = input_fname
        self._output_fname = output_fname

    def run(self):
        encoder = OoEncoder(self._config, self._language)
        encoder.decode(fname=str(self._input_fname), output_fname=str(self._output_fname))

