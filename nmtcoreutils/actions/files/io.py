import csv
import os
from abc import ABC, abstractmethod

from nmtcoreutils.actions.io import InputSource, OutputSource


class FileInputSource(InputSource):

    def __init__(self, fname):
        self._fname = fname

    @property
    def name(self):
        return self._fname

    @property
    def business(self):
        return os.path.getsize(self._fname)

    def get_next_input(self):
        for line in open(self._fname):
            yield line


class BulkPrinter(OutputSource, ABC):

    def __init__(self, file, max_queue_size=None, overwrite=True):
        self._max_queue_size = max_queue_size
        self._lines = list()
        self._file = file
        if overwrite and os.path.exists(self._file):
            os.remove(self._file)

    def provide_next_output(self, value):
        self.print(value)

    def finish(self):
        self.save()

    @property
    def name(self):
        return self.save_file

    @property
    def save_file(self):
        return self._file

    @property
    def business(self):
        return 0.0

    def print(self, line):
        self._lines.append(line)
        if self._max_queue_size and len(self._lines) > self._max_queue_size:
            self.save()

    def save(self):
        with open(self._file, 'a' if os.path.exists(self._file) else 'w', encoding='utf-8', newline='') as mf:
            self._print_lines(mf)
        self._lines = list()

    @abstractmethod
    def _print_lines(self, f_pointer):
        pass


class BasicBulkPrinter(BulkPrinter):

    def _print_lines(self, f_pointer):
        print(''.join(self._lines), file=f_pointer, end='')


class CsvBulkPrinter(BulkPrinter):

    def _print_lines(self, f_pointer):
        writer = csv.writer(f_pointer, delimiter=',', quotechar='"')
        writer.writerows(self._lines)
