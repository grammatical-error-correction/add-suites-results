import logging
import os

from nmtcoreutils.actions.base_actions import FileWriterAction
from nmtcoreutils.actions.files.io import BasicBulkPrinter


class SaveTopNOfFile(FileWriterAction):

    def __init__(self, data_files, source_language, target_language, save_folder, length=1000):
        super().__init__(save_folder=save_folder)
        self._source_language = source_language
        self._target_language = target_language
        self._data_files = data_files
        self._length = length

    def run(self):
        logging.info('Creating test set for files %s and %s', *self._data_files)

        if len(self._data_files) != 2:
            raise RuntimeError('Need data files of length 2')
        orig_files = set(self._data_files)
        base_name = self._data_files[0][:-len(str(self._source_language))]
        expected_files = [
            '{}{}'.format(base_name, str(self._source_language)),
            '{}{}'.format(base_name, str(self._target_language))
        ]
        if len(set(expected_files).intersection(orig_files)) != len(orig_files):
            raise RuntimeError('Original files are not a match {} != {}'.format(expected_files, orig_files))

        self._results = [
            self._create_test_set(base_name, expected_files[0], self._source_language),
            self._create_test_set(base_name, expected_files[1], self._target_language)
        ]

    def _create_test_set(self, base_name, data_file, lang):
        out = os.path.join(self._save_folder, '{}{}.test'.format(base_name, str(lang)))
        printer = BasicBulkPrinter(out, max_queue_size=self._length)

        logging.info('Creating test set for %s', data_file)
        it = 0
        for line in open(data_file):
            printer.print(line)
            it += 1
            if it == self._length:
                break
        printer.save()
        return out
