import gzip
import logging
import os

from nmtcoreutils.actions.base_actions import FileWriterAction
from nmtcoreutils.actions.files.io import BasicBulkPrinter


class UnzipAction(FileWriterAction):

    def __init__(self, save_folder, data_files):
        super().__init__(save_folder)
        self._data_files = data_files

    def run(self):
        self._results = list()
        logging.info('Starting to unzip')
        for file in self._data_files:
            logging.info('Unzip %s', file)
            fname = os.path.join(self._save_folder, os.path.split(file)[1][:-len('.gz')])
            printer = BasicBulkPrinter(fname, max_queue_size=500000)
            for line in gzip.open(file, 'rb'):
                printer.print(line.decode('utf-8'))
            printer.save()
            self._results.append(fname)
