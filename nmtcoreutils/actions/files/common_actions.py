from nmtcoreutils.actions.base_actions import FileWriterAction, Action


class NopAction(Action):

    def __init__(self, input_source):
        self._input_source = input_source

    def setup(self):
        pass

    def run(self):
        pass

    def cleanup(self):
        pass

    def results(self):
        return self._input_source


class IdentityAction(FileWriterAction):

    def __init__(self, save_folder, data_source, data_output):
        super().__init__(save_folder=save_folder)
        self._data_source = data_source
        self._data_output = data_output

    def run(self):
        for item in self._data_source.get_next_input():
            self._data_output.provide_next_output(item)
        self._data_output.finish()


class ConditionalAction(Action):

    def __init__(self, condition_fx, action1, action2):
        self._condition = condition_fx
        self._actions = {True: action1, False: action2}

    def setup(self):
        self._actions[self._condition()].setup()

    def run(self):
        self._actions[self._condition()].run()

    def cleanup(self):
        self._actions[self._condition()].cleanup()

    def results(self):
        self._actions[self._condition()].results()


class CreateFolderAction(FileWriterAction):

    def run(self):
        pass
