import os

from nmtcoreutils.actions.base_actions import FileWriterAction
from nmtcoreutils.actions.files.io import BasicBulkPrinter


class AlignMonolingualFiles(FileWriterAction):

    def __init__(self, save_folder, original_source, filtered_source, original_target, out_name):
        super().__init__(save_folder)
        self._original_source = original_source
        self._filtered_source = filtered_source
        self._original_target = original_target
        self._outname = out_name

    def run(self):
        index_map = self._get_index_map()
        self._apply_index_map(index_map)
        self._results.append(os.path.join(self.save_path, self._outname))

    def _get_index_map(self):
        index_map = list()
        src_idx = 0
        with open(self._original_source) as original_source_file:
            for tx_line in map(lambda l: tuple(l.strip()), open(self._filtered_source)):
                while tx_line != tuple(original_source_file.readline().strip()):
                    src_idx += 1
                index_map.append(src_idx)
                src_idx += 1
        return index_map

    def _apply_index_map(self, index_map):
        idx = 0
        printer = BasicBulkPrinter(os.path.join(self.save_path, self._outname), max_queue_size=100000)
        for line in open(self._original_target):
            if idx == index_map[0]:
                printer.print(line)
                index_map.pop(0)
            idx += 1
            if len(index_map) == 0:
                break
        printer.save()
