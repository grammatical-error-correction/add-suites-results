import os
import shutil
import subprocess

from nmtcoreutils.actions.base_actions import Action


class VirtualEnvAction(Action):

    def __init__(self, setup_location, install_requirements=False, version=3):
        self._setup_location = setup_location
        self._requirements = install_requirements
        self._version = version

    def setup(self):
        pass

    def run(self):
        subprocess.call('/bin/bash -c "virtualenv -p python{} ."'.format(self._version), cwd=self._setup_location, shell=True)
        if self._requirements:
            subprocess.call('/bin/bash -c "source bin/activate; pip install -r requirements.txt; deactivate"',
                            cwd=self._setup_location, shell=True)

    def cleanup(self):
        shutil.rmtree(os.path.join(self._setup_location, 'bin'))
        shutil.rmtree(os.path.join(self._setup_location, 'lib'))
        shutil.rmtree(os.path.join(self._setup_location, 'include'))

    def results(self):
        return self._setup_location
