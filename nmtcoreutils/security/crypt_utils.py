from enum import Enum


from Cryptodome import Random
from Cryptodome.Cipher import PKCS1_OAEP
from Cryptodome.Hash import SHA512, SHA384, SHA256, SHA, MD5
from Cryptodome.PublicKey import RSA
from Cryptodome.Signature import PKCS1_v1_5



class DigestAlgos(Enum):
    SHA_256 = SHA256
    SHA_1 = SHA
    SHA_384 = SHA384
    MD_5 = MD5
    SHA_512 = SHA512

    def get(self):
        return self.value


def get_key(path):
    with open(path) as mf:
        return mf.read()


def newkeys(keysize):
    random_generator = Random.new().read
    key = RSA.generate(keysize, random_generator)
    private, public = key, key.publickey()
    return public, private


def import_key(extern_key):
    return RSA.importKey(extern_key)


def get_public_key(priv_key):
    return priv_key.publickey()


def encrypt(message, pub_key):
    # RSA encryption protocol according to PKCS#1 OAEP
    cipher = PKCS1_OAEP.new(pub_key)
    return cipher.encrypt(message)


def decrypt(ciphertext, priv_key):
    # RSA encryption protocol according to PKCS#1 OAEP
    cipher = PKCS1_OAEP.new(priv_key)
    return cipher.decrypt(ciphertext)


def sign(message, priv_key, digest_algo=SHA256):
    signer = PKCS1_v1_5.new(priv_key)
    digest = digest_algo.new()
    digest.update(message)
    return signer.sign(digest)


def verify(message, signature, pub_key, digest_algo=SHA256):
    signer = PKCS1_v1_5.new(pub_key)
    digest = digest_algo.new(message)
    return signer.verify(digest, signature)  # pylint: disable=not-callable
