import json
import os
from base64 import b64decode, b64encode
from pathlib import Path

from nmtcoreutils.security import crypt_utils

SECURITY_FILE_PATH = os.path.join(os.path.join(str(Path.home()), '.nmt_credentials', 'credentials'))
DEFAULT_KEY = os.path.join(str(Path.home()), '.ssh', 'id_rsa')


def get_key(location=None):
    return crypt_utils.import_key(crypt_utils.get_key(location if location else DEFAULT_KEY))


def load_credentials_file(key, file_path=None):
    file_path = file_path if file_path else SECURITY_FILE_PATH
    if os.path.exists(file_path):
        with open(file_path, 'rb') as mf:
            data = b64decode(mf.read())
            return json.loads(crypt_utils.decrypt(data, key).decode('ascii'))
    return dict()


def save_credentials_file(credentials, key, file_path=None):
    file_path = file_path if file_path else SECURITY_FILE_PATH
    os.makedirs(os.path.split(file_path)[0], exist_ok=True)
    data = b64encode(crypt_utils.encrypt(json.dumps(credentials).encode('ascii'), key))
    with open(file_path, 'wb') as mf:
        mf.write(data)


def save_new_credentials(name, params, key_location=None, file_path=None):
    key = get_key(key_location)
    credentials_file = load_credentials_file(key, file_path=file_path)
    credentials_file[name] = params
    save_credentials_file(credentials_file, key, file_path=file_path)


def get_credentials_for_name(name, key_location=None):
    try:
        key = get_key(key_location)
        credentials_file = load_credentials_file(key)
        credentials = credentials_file.get(name, None)
        del credentials_file
        return credentials
    except Exception:  # pylint: disable=broad-except
        return None
