import argparse
import logging
import os
from argparse import ArgumentParser
from enum import Enum

from nmtcoreutils.core import Languages

RELEASES_PATH = os.path.join("/", "devdata", "releases")

RELEASES_MODEL_PATH = "model-deployments"
RELEASES_MODEL_MODEL_PATH = "model"
RELEASES_MODEL_TRAINING_DATA_PATH = "data"
RELEASES_MODEL_TRAINING_SETS_PATH = "training_sets"
RELEASES_MODEL_EXPORT_PATH = "export"
RELEASES_MODEL_TOKENIZER_PATH = "tokenizer"


def get_arg_or_default(args: argparse.Namespace, string, default=None):
    return vars(args).get(str(string), default)


def get_language(args, language_var):
    val = get_arg_or_default(args, language_var)
    return Languages.get_enum_by_value(val) if val else val


class Vars(Enum):
    SourceLanguage = 'source_language'
    TargetLanguage = 'target_language'
    GPUs = 'gpus'
    Workspace = 'workspace'
    ActionCleanup = 'cleanup'
    FlatWorkspace = 'flat_workspace'
    ModelPath = 'model_path'
    ServablePath = 'servable_path'
    ModelName = 'model_name'
    ModelVersion = 'model_version'
    Port = 'port'
    Stage = 'stage'
    ReleasePath = 'release_path'

    def __str__(self):
        return self.value

    @staticmethod
    def get_var(args, var):
        special_cases = {
            str(Vars.SourceLanguage): lambda: get_language(args, var),
            str(Vars.TargetLanguage): lambda: get_language(args, var),
        }
        return special_cases[str(var)]() if str(var) in special_cases else get_arg_or_default(args, var)


class ArgBuilder:

    def __init__(self, msg):
        self._parser = ArgumentParser(msg)
        self._args = list()
        self._parsed_args = None
        self._shorthands = set()

    def with_source_language(self):
        return self._add_argument(Vars.SourceLanguage, '-s', '--source-language', help='Source language', required=True,
                                  choices=[l.value for l in Languages], type=str)

    def with_target_language(self):
        return self._add_argument(Vars.TargetLanguage, '-t', '--target-language', help='Target language', required=True,
                                  choices=[l.value for l in Languages], type=str)

    def with_gpus(self, default=''):
        return self._add_argument(Vars.GPUs, '-gpu', '--gpus', help='GPUs', required=False, default=default, type=str)

    def with_workspace(self):
        return self._add_argument(Vars.Workspace, '-w', '--workspace', help='Workspace', required=True, type=str)

    def with_action_cleanup(self):
        return self._add_argument(
            Vars.ActionCleanup, '-c', '--cleanup', help='cleanup', required=False, action='store_true')

    def with_flat_workspace(self):
        return self._add_argument(
            Vars.FlatWorkspace, '-sw', '--single-workspace', help='Single workspace mode', action='store_true')

    def with_model_path(self, default=os.path.join('/', 'data', 'training')):
        return self._add_argument(Vars.ModelPath, '-m', '--model-path', help='path to model', type=str, required=False,
                                  default=default)

    def with_servable_path(self):
        return self._add_argument(Vars.ServablePath, '-o', '--servable-path', help='path to servables', type=str,
                                  required=False, default=os.path.join('/', 'data', 'models'))

    def with_release_path(self, release_project_name):
        return self._add_argument(
            Vars.ReleasePath, '--release-path', help="Release path", required=False,
            default=os.path.join(RELEASES_PATH, release_project_name))

    def with_dev_data_model_path(self):
        return self.with_release_path(RELEASES_MODEL_PATH)

    def with_model_name(self, default='reverso'):
        return self._add_argument(
            Vars.ModelName, '--modelname', help='Name of model to use', required=False, type=str, default=default)

    def with_model_version_to_use(self):
        return self._add_argument(
            Vars.ModelVersion, '-v', '--version', help='Version of model to use', required=False, type=str,
            default=None)

    def with_port(self, default):
        return self._add_argument(Vars.Port, '-p', '--port', help='Port', required=False, default=default, type=int)

    def with_stage(self, default="prod"):
        return self._add_argument(Vars.Stage, '-x', '--deployment-stage', required=False, default=default, type=str)

    def with_custom(self, dest, *args, **kwargs):
        return self._add_argument(dest, *args, **kwargs)

    def _add_argument(self, dest, shorthand, *args, **kwargs):
        if shorthand in self._shorthands:
            logging.error(
                'Unable to add argument %s with shorthand %s since that flag is already use!', dest, shorthand)
            return self
        self._args.append(dest)
        self._parser.add_argument(shorthand, *args, dest=str(dest), **kwargs)
        self._shorthands.add(shorthand)
        return self

    @property
    def parsed_args(self):
        if not self._parsed_args:
            self._parsed_args = self.get_parser().parse_args()
        return self._parsed_args

    def get_parser(self):
        return self._parser

    def get_set_args(self):
        return [Vars.get_var(self.parsed_args, arg) for arg in self._args]
