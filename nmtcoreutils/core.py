

from enum import Enum


class Languages(Enum):
    EN = 'en'
    FR = 'fr'
    ES = 'es'
    IT = 'it'
    DE = 'de'
    PT = 'pt'
    RU = 'ru'
    AR = 'ar'
    RO = 'ro'
    NL = 'nl'

    @staticmethod
    def get_enum_by_value(value):
        for e in Languages:
            if e.value == value:
                return e
        return None

    def __str__(self):
        return self.get_2_character_encoding()

    def get_2_character_encoding(self):
        return self.value

    def get_3_character_encoding(self):
        return {
            Languages.EN: 'eng',
            Languages.FR: 'fra',
            Languages.ES: 'spa',
            Languages.IT: 'ita',
            Languages.DE: 'deu',
            Languages.PT: 'por',
            Languages.RU: 'rus',
            Languages.AR: 'ara',
            Languages.RO: 'ron',
            Languages.NL: 'nld',
        }[self]
