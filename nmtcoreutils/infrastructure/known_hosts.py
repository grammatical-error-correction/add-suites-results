from enum import Enum


class KnownHosts(Enum):
    DEV2 = 'dev2'
    DEV5 = 'dev5'
    DEV6 = 'dev6'
    DEVTEST = 'devtest'
    DEV10 = 'dev10'
    BST4 = 'bst4'
    BST3 = 'bst3'

    def get_host_name(self):
        return {
            KnownHosts.DEV2: 'dev-oo2.ovh.reverso.net',
            KnownHosts.DEV5: 'dev-oo5.ovh.reverso.net',
            KnownHosts.DEV6: 'dev-oo6.ovh.reverso.net',
            KnownHosts.DEVTEST: 'dev-test.dev.tradooit.com',
            KnownHosts.DEV10: '51.75.15.114',
            KnownHosts.BST4: 'dev-bst4.context.reverso.net',
            KnownHosts.BST3: 'dev-bst3.context.reverso.net'
        }[self]

    @staticmethod
    def from_string_value(val):
        for host in KnownHosts:
            if val == host.value:
                return host
        return None
