from typing import List, Optional

from nmtcoreutils.api.api_requests import ApiRequest
from nmtcoreutils.api.nmt.translations import TranslationText
from nmtcoreutils.api.service import WebService
from nmtcoreutils.core import Languages


class TradooitNMTService(WebService):

    def __init__(self, host, port, api_base=None, https=False):
        super().__init__('TradooitService', host, port)
        self._api_base = api_base
        self._https = https

    def infer_language(self, text) -> Optional[Languages]:
        params = dict()
        params['text'] = text
        request = ApiRequest(self, 'langdetect', params=params, is_get=True)
        result, _ = request.result()
        if len(result) == 0:
            return None
        return Languages.get_enum_by_value(max(result, key=lambda l: l['prob'])['lang'])

    def post_translate(self, translation_texts: List[TranslationText], context='Prod') -> ApiRequest:

        params = dict()
        params['context'] = context
        params['format'] = 'TEXT'
        encoded_texts = list()
        for text in translation_texts:
            if 'langfrom' not in params:
                params['langfrom'] = str(text.source_language)
                params['langto'] = str(text.target_language)
            elif params['langfrom'] != str(text.source_language) or params['langto'] != str(text.target_language):
                raise ValueError('Text must all have same source and dest language')

            encoded = text.encode()
            encoded.pop('langfrom')
            encoded.pop('langto')
            encoded_texts.append(encoded)
        params['sources'] = encoded_texts
        return ApiRequest(self, 'translate', params=params, is_get=False)

    def get_translate(self, translation_text, context='Prod') -> ApiRequest:
        params = dict()
        params['context'] = context
        params.update(translation_text.encode())
        return ApiRequest(self, 'translate', params=params, is_get=True)
