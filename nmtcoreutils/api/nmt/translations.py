from abc import ABC, abstractmethod

from nmtcoreutils.api.nmt.constraints import get_constraint_from_yaml
from nmtcoreutils.core import Languages


class TranslationText(ABC):

    @abstractmethod
    def encode(self):
        pass

    @property
    @abstractmethod
    def text(self):
        pass

    @property
    @abstractmethod
    def source_language(self):
        pass

    @property
    @abstractmethod
    def target_language(self):
        pass


class StandardTranslation(TranslationText):

    def __init__(self, text, source_lang, target_lang):
        self._text = text
        self._source_lang = source_lang
        self._target_lang = target_lang

    @property
    def text(self):
        return self._text

    @property
    def source_language(self):
        return self._source_lang

    @property
    def target_language(self):
        return self._target_lang

    def encode(self):
        return {
            'source': self.text,
            'langfrom': str(self.source_language),
            'langto': str(self.target_language)
        }


class ConstrainedTranslation(StandardTranslation):

    def __init__(self, text, source_lang, target_lang, constraints):
        super().__init__(text, source_lang, target_lang)
        self._constraints = list(constraints)

    def add_constraint(self, constraint):
        self._constraints.append(constraint)

    def encode(self):
        base = super().encode()
        base['constraints'] = [cons.encode() for cons in self._constraints]
        return base


def get_translation_text_from_yaml(yamled):
    trans_type = StandardTranslation
    kwargs = dict()
    kwargs['text'] = yamled['text']
    kwargs['source_lang'] = Languages.get_enum_by_value(yamled['source language'])
    kwargs['target_lang'] = Languages.get_enum_by_value(yamled['target language'])
    if 'constraints' in yamled:
        trans_type = ConstrainedTranslation
        kwargs['constraints'] = [get_constraint_from_yaml(cons_yaml) for cons_yaml in yamled['constraints']]
    return trans_type(**kwargs)
