from enum import Enum


class ConstraintType(Enum):
    DO_NOT_USE = 'AVOID'
    USE = 'NONE'

    def __str__(self):
        return str(self.value)

    @staticmethod
    def from_text(val):
        for x in ConstraintType:
            if str(x).lower() == str(val).lower():
                return x
        raise ValueError('Val {} is not an enum'.format(val))


def get_constraint_from_yaml(yamled):
    return Constraint(ConstraintType.from_text(yamled['type']), yamled['source'], yamled['target'])


class Constraint:

    def __init__(self, constraint_type, source, target):
        self._constraint_type = constraint_type
        self._source = source
        self._target = target

    def encode(self):
        return {
            'mode': self._constraint_type.value,
            'source': self._source,
            'target': self._target
        }
