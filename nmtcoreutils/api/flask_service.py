import logging
from abc import ABC, abstractmethod
from enum import Enum
from typing import Iterable, Dict

from flask import Flask

from nmtcoreutils.api.service import WebService


class RestMethods(Enum):
    GET = 'GET'
    POST = 'POST'
    DELETE = 'DELETE'
    PATCH = 'PATCH'

    def __repr__(self):
        return str(self)

    def __str__(self):
        return self.value


class FlaskUrlRule:

    def __init__(self, rule, endpoint, function=None, methods: Iterable[RestMethods] = None):
        self._rule = rule
        self._endpoint = str(endpoint)
        self._function = function if function else endpoint
        self._methods = methods if methods else [RestMethods.GET]

    def as_args(self) -> Dict:
        return {
            'rule': self._rule,
            'endpoint': self._endpoint,
            'view_func': self._function,
            'methods': [str(m) for m in self._methods]
        }


class FlaskService(WebService, ABC):

    def __init__(self, name, host, port, https=False, with_session=False, request_logging=True):
        super().__init__(name, host, port, https=https, with_session=with_session)
        self._app = Flask(name)
        if not request_logging:
            log = logging.getLogger('werkzeug')
            log.disabled = True
            self._app.logger.disabled = True

    def start_flask_service(self):
        for url_rule in self.get_url_rules():
            self._app.add_url_rule(**url_rule.as_args())
        self._app.run(host=self.host, port=self.port, debug=False, use_reloader=False, threaded=True)

    @abstractmethod
    def get_url_rules(self) -> Iterable[FlaskUrlRule]:
        pass
