import getpass
import json
import os
from enum import Enum
from http import HTTPStatus

from nmtcoreutils.api.api_requests import ApiRequest, Request
from nmtcoreutils.api.mtcompare.evaluation import SourceEvaluation
from nmtcoreutils.api.service import WebService
from nmtcoreutils.core import Languages
from nmtcoreutils.security import credentials


def convert_language_to_mtcompare(language):
    return {
        Languages.ES: 'sp'
    }.get(language, language.value)


def convert_language_pair_to_direction(source_language: Languages, target_language: Languages):
    converted_source_language = convert_language_to_mtcompare(source_language)
    converted_target_language = convert_language_to_mtcompare(target_language)
    if not converted_source_language or not converted_target_language:
        raise RuntimeError('Unknown MT compare language {}'.format(
            source_language if not converted_source_language else converted_target_language))
    return '{}-{}'.format(converted_source_language, converted_target_language)


class MtCompareServiceFactory(Enum):
    # pylint: disable=unnecessary-lambda
    Default = 'default'
    Prod = 'prod'

    def get_service(self, **kwargs):
        return {
            self.Default: lambda: MtCompareServiceFactory.Prod.get_service(**kwargs),
            self.Prod: lambda: MtCompareService('mtcompare.reverso.net', 80, **kwargs)
        }[self]()


class MtCompareService(WebService):
    DEFAULT_API_BASE_PATH = 'tsapi'

    def __init__(self, host, port, https=False, base_api_path=None, **kwargs):
        super().__init__('MtCompare', host, port, https=https, **kwargs)
        self._base_api_path = base_api_path if base_api_path else MtCompareService.DEFAULT_API_BASE_PATH

    def _api_format(self, *args):
        args = [str(arg) for arg in args]
        return os.path.join(self._base_api_path, *args)

    def api_version(self) -> Request:
        return ApiRequest(self, self._api_format('Version'))

    def login(self, username: str, password: str) -> Request:
        return ApiRequest(self, self._api_format('Login?username={}&password={}'.format(username, password)),
                          is_get=False)

    def logout(self) -> Request:
        return ApiRequest(self, self._api_format('Logout'))

    def get_categories(self) -> Request:
        return ApiRequest(self, self._api_format('GetCategories'))

    def get_test_suites(self, source_language: Languages, target_language: Languages) -> Request:
        direction = convert_language_pair_to_direction(source_language, target_language)
        return ApiRequest(self, self._api_format('GetTestSuites', direction))

    def put_test_suite(self, guid: str, name: str, friendly_name: str, source_language: Languages,
                       target_language: Languages) -> Request:
        direction = convert_language_pair_to_direction(source_language, target_language)
        return ApiRequest(self, self._api_format('PutTestSuite', {
            'Id': guid, 'Name': name, 'FriendlyName': friendly_name, 'Direction': direction
        }))

    def add_test(self, guid: str, test) -> Request:
        params = dict()
        params['Id'] = guid
        params.update(test.to_params())
        return ApiRequest(self, self._api_format('AddTest', guid), params=params, is_get=False)

    def delete_test(self, guid: str) -> Request:
        return ApiRequest(self, self._api_format('DeleteTest', guid))

    def get_sources(self, guid: str) -> Request:
        return ApiRequest(self, self._api_format('GetSources', guid))

    def put_results(self, source_evaluation: SourceEvaluation, description=None, tag=None) -> Request:
        return ApiRequest(self, self._api_format('PutResults'), {
            'Description': description if description else '',
            'Tag': tag if tag else '',
            'Items': [source_evaluation.to_params()]
        }, is_get=False)

    def get_result_sets_for_test_guid(self, test_suit_guid) -> Request:
        return ApiRequest(self, self._api_format('GetResultsSets', test_suit_guid))

    def delete_result_set(self, result_set_guid):
        return ApiRequest(self, self._api_format('DeleteResultsSet', result_set_guid))

    def delete_all_results_for_test_guid(self, test_guid) -> Request:
        return ApiRequest(self, self._api_format('DeleteTestSuiteResults', test_guid))

    def delete_result(self, *result_guids) -> Request:
        return ApiRequest(
            self, self._api_format('DeleteResults'), is_get=False, params=list(result_guids)
        )

    def get_results_from_result_set(self, result_set_guid, only_id=True) -> Request:
        return ApiRequest(self, self._api_format('GetResults', result_set_guid), params={
            'onlyId': only_id
        })

    def get_all_results(self, test_suite_guid, only_id=True) -> Request:
        return ApiRequest(self, self._api_format('GetAllResults', test_suite_guid), params={
            'onlyId': only_id
        })

    def get_score(self, result_guid) -> Request:
        return ApiRequest(self, self._api_format('GetScore', result_guid))


def login_with_credentials(service):
    args = credentials.get_credentials_for_name('mt_compare')
    if args:
        username, password = args['username'], args['password']
    else:
        username = input('mt_compare username: ')
        password = getpass.getpass('mt_compare password: ')

    try:
        _, code = service.login(username=username, password=password).result()
        return code == HTTPStatus.OK
    except json.decoder.JSONDecodeError:
        return False
