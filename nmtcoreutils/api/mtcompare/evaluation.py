import logging
import os
from enum import Enum
from typing import Dict

from nmtcoreutils.core import Languages


class TestSuiteFactory:

    @staticmethod
    def get_test_suite_from_guid_and_name(guid, name):
        return TestSuite(guid=guid, name=name)

    @staticmethod
    def infer_test_suite(service, source_language: Languages, target_language: Languages):
        default_name = 'evaluation-{}-{}'.format(str(source_language), str(target_language))
        return TestSuiteFactory.get_test_suit_from_name(service, source_language, target_language, default_name)

    @staticmethod
    def get_test_suit_from_name(service, source_language: Languages, target_language: Languages, test_name):
        request = service.get_test_suites(source_language, target_language)
        test_suites, _ = request.result()
        for test_suite in test_suites:
            if test_suite['Name'] == test_name:
                return TestSuite(guid=test_suite['Id'], name=test_suite['Name'])
        return None


class TestSuite:

    def __init__(self, guid, name):
        self._name = name
        self._guid = guid

    @property
    def guid(self):
        return self._guid

    @property
    def name(self):
        return self._name


def replace_all(string: str, token_map: dict):
    for old, new in token_map.items():
        string = string.replace(old, new)
    return string


def prepare_mt_compare_source(text):
    try:
        with open(os.devnull, 'w', encoding='utf-8') as mf:
            print(text, file=mf)
    except (UnicodeEncodeError, UnicodeDecodeError):
        logging.exception('Is not proper unicode: %s', text)
        raise UnicodeError()

    return replace_all(text, {
        '[': '',
        ']': '',
        '\r\n': ' ',
        '\n': ' '
    })


class ResultSet:

    def __init__(self, guid, created, version, tag, description):
        self.guid = guid
        self.created = created
        self.version = version
        self.tag = tag
        self.description = description

    @staticmethod
    def decode(json):
        conversion = {
            'Id': 'guid',
            'DateCreated': 'created',
            'Version': 'version',
            'Tag': 'tag',
            'Description': 'description'
        }
        kwargs = {conversion[key]: json[key] for key, val in conversion.items()}
        return ResultSet(**kwargs)


class Source:

    def __init__(self, guid, source, version, category):
        self._guid = guid
        self._source = prepare_mt_compare_source(source)
        self._version = version
        self._category = category

    @property
    def guid(self):
        return self._guid

    @property
    def source(self):
        return self._source

    @property
    def version(self):
        return self._version

    @property
    def category(self):
        return self._category

    @staticmethod
    def decode(jsoned):
        return Source(
            guid=jsoned['Id'], source=jsoned['Source'], version=jsoned['Version'], category=jsoned['Category'])

    def encode(self) -> dict:
        return {
            'Id': self._guid,
            'Source': self._source,
            'Version': self._version,
            'Category': self._category
        }


class SourceEvaluationItem:

    def __init__(self, source_guid, source_version, translation):
        self._guid = source_guid
        self._version = source_version
        self._translation = translation

    def to_params(self):
        return {
            'SourceId': self._guid,
            'SourceVersion': self._version,
            'Output': self._translation
        }


class SourceEvaluation:

    def __init__(self, test_suite_id):
        self._guid = test_suite_id
        self._results = list()

    def add_source_evaluation(self, item: SourceEvaluationItem):
        self._results.append(item)

    def to_params(self):
        return {
            'TestSuiteId': self._guid,
            'Results': [item.to_params() for item in self._results]
        }


class EvaluationCategory(Enum):
    Spelling = 'Spelling'
    Grammar = 'Grammar'
    IdiomAndExpression = 'Idiom&Expression'
    Mistranslation = 'Mistranslation'
    NamedEntityAndAccronym = 'Named Entity & Acronym'
    OverUnderTranslation = 'Over/Under Translation'
    Syntax = 'Syntax'
    Other = None

    def __str__(self):
        return self.value

    @staticmethod
    def from_value(value):
        for c in EvaluationCategory:
            if c.value == value:
                if c.value is None or value is None:
                    return c
                if c.value.lower().rstrip().lstrip() == value.lower().rstrip().lstrip():
                    return c

        new_name = ''.join([str(ord(c)) for c in value])
        setattr(EvaluationCategory, new_name, value)
        return getattr(EvaluationCategory, new_name, value)


class EvaluationScore:

    def __init__(self, score, distance, samples, category=None, guid=None):
        self._guid = guid
        self._score = score
        self._distance = distance
        self._category = category
        self._samples = samples

    @property
    def guid(self):
        return self._guid

    @property
    def score(self):
        return self._score

    @property
    def distance(self):
        return self._distance

    @property
    def category(self) -> EvaluationCategory:
        return self._category

    @property
    def samples(self):
        return self._samples

    @property
    def is_top_level(self):
        return not self.category


def decode_evaluation_score_api_request(jsoned) -> (EvaluationScore, Dict[EvaluationCategory, EvaluationScore]):
    scores_by_category = dict()

    for category in jsoned['ByCategory']:
        cat = EvaluationCategory.from_value(category['Category'])
        scores_by_category[cat] = EvaluationScore(
            distance=float(category['Distance']), score=float(category['Score']), samples=int(category['Total']),
            category=cat,
        )

    top_level = EvaluationScore(
        guid=jsoned['Id'], score=jsoned['Score'], distance=jsoned['Distance'],
        samples=sum(map(lambda k: scores_by_category[k].samples, scores_by_category.keys()))
    )

    return top_level, scores_by_category
