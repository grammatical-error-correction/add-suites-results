
from abc import ABC, abstractmethod

import requests


class Service(ABC):

    @property
    @abstractmethod
    def name(self):
        pass

    def endpoint(self):
        return self.host, self.port

    @property
    @abstractmethod
    def port(self):
        pass

    @property
    @abstractmethod
    def host(self):
        pass


def format_api_endpoint(host, port, api):
    endpoint = 'http://{}:{}/'.format(host, port)
    return '{}{}'.format(endpoint, api)


class WebService(Service):

    def __init__(self, name, host, port, https=False, with_session=False):
        self._name = name
        self._host = host
        self._port = port
        self._https = https
        self._session = requests.Session() if with_session else None

    @property
    def name(self):
        return self._name

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    @property
    def session(self):
        return self._session

    def get_url(self, api=None):
        return '{}://{}:{}/{}'.format('https' if self._https else 'http', self.host, self.port, api if api else '')
