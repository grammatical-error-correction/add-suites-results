import logging
import time
from threading import Thread
from typing import Iterable

from flask import request

from nmtcoreutils.actions.base_actions import Action
from nmtcoreutils.api.flask_service import FlaskService, FlaskUrlRule, RestMethods


class ActionManagerService(FlaskService):

    def __init__(self, name, host, port, action: Action, auth_key=None, https=False, with_session=False):
        super().__init__(name=name, host=host, port=port, https=https, with_session=with_session)
        self._action = action
        self._process = None
        self._probe_time_s = 15
        self._graceful_stop_retry = 8
        self._auth_key = auth_key

    def get_url_rules(self) -> Iterable[FlaskUrlRule]:
        return [
            FlaskUrlRule('/start', '/start', function=self.start, methods=[RestMethods.GET]),
            FlaskUrlRule('/stop', '/stop', function=self.stop, methods=[RestMethods.GET]),
            FlaskUrlRule('/restart', '/restart', function=self.restart, methods=[RestMethods.GET])
        ]

    def start(self):
        if not self._is_request_valid():
            return 'Bad auth key', 403

        logging.info('Received command to start the process')
        if self._is_process_alive():
            return 'Process is already started', 400

        self._process = Thread(target=self._action.run)
        self._process.start()
        time.sleep(1)
        if not self._is_process_alive():
            return 'Process failed to start', 500

        return 'Process started', 200

    def stop(self):
        if not self._is_request_valid():
            return 'Bad auth key', 403

        logging.info('Received command to stop the process')
        if not self._is_process_alive():
            return 'Process is already not running', 400

        self._action.exit()

        count = 0
        while self._is_process_alive() and count < self._graceful_stop_retry:
            logging.info('Checking if service stopped (%s)', count)
            self._process.join(self._probe_time_s)
            count += 1

        if self._is_process_alive():
            return 'Failed to stop process', 500

        return 'Process succesfully stopped', 200

    def restart(self):
        if not self._is_request_valid():
            return 'Bad auth key', 403

        logging.info('Received command to restart the process')
        msg, code = self.stop()
        if self._is_process_alive():
            return msg, code

        msg, code = self.start()
        if not self._is_process_alive():
            return msg, code

        return 'Process restarted successfully', 200

    def _is_request_valid(self):
        return request.args.get('authkey', None) == self._auth_key

    def _is_process_alive(self):
        return self._process and self._process.is_alive()
