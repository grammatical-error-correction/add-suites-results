import json
import logging
import time
from abc import ABC, abstractmethod
from xml.etree import ElementTree

import requests


def retry_request(lambda_request_fx, times=5):
    for i in range(times):
        try:
            result = lambda_request_fx().result()
        except json.decoder.JSONDecodeError:
            result = None
        if result:
            return result
        time.sleep(2 ** i)

    raise RuntimeError('Request failed {} times'.format(times))


class Request(ABC):

    @abstractmethod
    def result(self, **kwargs):
        pass


class BasicRequest(Request):

    def __init__(self, web_service, api, params=None, is_get=True):
        self._web_service = web_service
        self._api = api
        self._params = params if params else dict()
        self._is_get = is_get
        self._result = None

    @property
    def web_service(self):
        return self._web_service

    def result(self, **kwargs):
        no_cache = kwargs.get('no_cache', False)
        if not self._result or no_cache:
            calling_entity = requests if not self._web_service.session else self._web_service.session
            if self._is_get:
                self._result = calling_entity.get(
                    self.web_service.get_url(self._api), params=self._params)
            else:
                self._result = calling_entity.post(
                    self.web_service.get_url(self._api), json=self._params)
        return self._result


class ApiRequest(BasicRequest):

    def result(self, **kwargs):
        result = super().result(**kwargs)
        try:
            return (result.json(), result.status_code) if result.text else (None, result.status_code)
        except json.decoder.JSONDecodeError as e:
            logging.error('Got exception for decoding: %s', result.text)
            raise ApiRequestError(e)


class XmlRequest(BasicRequest):

    def result(self, **kwargs) -> ElementTree:
        result = super().result(**kwargs)
        return ElementTree.fromstring(result.content.decode('utf-8')), result.status_code


class ApiRequestError(RuntimeError):
    pass
