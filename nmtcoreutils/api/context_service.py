from typing import Dict

from nmtcoreutils.api.api_requests import ApiRequest
from nmtcoreutils.api.service import WebService
from nmtcoreutils.core import Languages


class ContextService(WebService):

    def __init__(self):
        super().__init__(name="Context", host="preprod.context.reverso.net", port=8091, https=False)

    def query(self, source_language: Languages, target_language: Languages, source_text, json=1) -> ApiRequest:
        return ApiRequest(self, 'bst-query-service', {
            'source_text': source_text,
            'source_lang': str(source_language),
            'target_lang': str(target_language),
            'json': json
        })


def get_hits_for_word(source_language, target_language, word) -> Dict[str, int]:
    tradooit = ContextService()
    result = tradooit.query(source_language, target_language, word).result()
    return {entry['term']: int(entry['frequency']) for entry in result['dictionary_entry_list']}
