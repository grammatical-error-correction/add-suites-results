from enum import Enum
from typing import Dict

from nmtcoreutils.api.api_requests import XmlRequest
from nmtcoreutils.api.service import WebService


class KnownTradooitSources(Enum):
    PARLGCCA = 'parlgcca'
    HANSARD = 'hansard'
    NEWSGCCA = 'newsgcca'
    MOVIE = 'movie'
    EMEA = 'emea'
    BREVET = 'brevet'
    SENGCCA = 'sengcca'
    EUROPARL = 'europarl'
    CORDIS = 'cordis'
    NRC = 'nrc'
    UNESCO = 'unesco'
    CINEUROPA = 'cineuropa'
    VAR = 'var'
    WORLDBANK = 'worldbank'
    VCOM = 'vcom'
    TSBGCCA = 'tsbgcca'
    ECGCCA = 'ecgcca'
    THEBAY = 'thebay'
    NEWSONCA = 'newsonca'
    NEBGCCA = 'nebgcca'
    LOIONT = 'loiont'
    HRDC_NOC = 'hrdc_noc'
    CCOHS = 'ccohs'
    HRSDC_SKILLS = 'hrsdc_skills'
    STATCAN = 'statcan'
    OECD = 'oecd'
    VATICAN = 'vatican'
    BLOODCA = 'bloodca'
    USAGOV = 'usagov'
    TED = 'ted'
    UNAIDS = 'unaids'
    SCIENCE = 'science'
    HCSCCA = 'hcscca'
    PMPGCCA = 'pmpgcca'
    ICGCCA = 'icgcca'
    WEDGCCA = 'wedgcca'
    EUROPARL2 = 'europarl2'
    COSTCO = 'costco'
    THOMAS = 'thomas'
    JUSTICEGCCA = 'justicegcca'
    AGRCA = 'agrca'
    VNEWS = 'vnews'
    TPSGCCA = 'tpsgcca'
    TBSGCCA = 'tbsgcca'
    GRAIN = 'grain'
    RICARDO = 'ricardo'
    CCCB = 'cccb'
    VHOSPICE = 'vhospice'
    INTERGCCA = 'intergcca'
    RELIGION = 'religion'
    UNICEF = 'unicef'
    CNSCGCCA = 'cnscgcca'
    LEGAL = 'legal'
    CWM = 'cwm'
    ESOEU = 'esoeu'
    OURPLANET = 'ourplanet'
    PREPGCCA = 'prepgcca'
    OTTAWAPOLICE = 'ottawapolice'
    DFC = 'dfc'
    RESPECT = 'respect'
    DRCA = 'drca'
    FCMCA = 'fcmca'
    TCUONCA = 'tcuonca'
    MWNF = 'mwnf'
    SOSCUISINE = 'soscuisine'


class TradooitService(WebService):

    def __init__(self):
        super().__init__(name="Tradooit", host="www.tradooit.com", port=80, https=False)

    def query(self, query, max_result=1000, remove_sources=None) -> XmlRequest:
        params = {'max': max_result, 'query': query}
        if remove_sources:
            params['query'] = '{} source:{}'.format(params['query'], ','.join(map('-{}'.format, remove_sources)))

        return XmlRequest(self, 'search/Search', params)


def get_hits_for_word(word, remove_sources=None) -> Dict[str, int]:
    tradooit = TradooitService()
    root, _ = tradooit.query(word, remove_sources=remove_sources).result()
    try:
        element = root.find('filters').find('filter')
    except TypeError:
        return dict()
    if not element:
        return dict()
    return {alternatives.text: int(alternatives.attrib['count']) for alternatives in element}
