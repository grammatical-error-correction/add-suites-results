import json
import logging
import os
import pathlib
import shutil
from abc import abstractmethod, ABC

from nmtcoreutils.actions.action_thread import ActionThread
from nmtcoreutils.actions.base_actions import Action
from nmtcoreutils.actions.code.git import GitSetupAction
from nmtcoreutils.actions.containers.venv import VirtualEnvAction
from nmtcoreutils.actions.files.common_actions import CreateFolderAction


class PathSubclass(pathlib.Path):
    # pylint: disable=no-member
    _flavour = pathlib.Path('.')._flavour


class File(PathSubclass):

    def operation(self, ops_fn, mode='r', **kwargs):
        with open(str(self), mode=mode, **kwargs) as mp:
            return ops_fn(mp)

    def is_file(self):
        return True

    def create(self):
        os.makedirs(str(os.path.split(str(self))[0]), exist_ok=True)

    def delete(self):
        os.remove(str(self))

    def write(self, text):
        self.operation(lambda fp: print(text, file=fp), mode='w')

    def read(self):
        return self.operation(lambda fp: [line.strip() for line in fp])


class JSONFile(File):

    def load(self, default=None):
        if not self.exists():
            return default
        return self.operation(lambda fp: json.load)

    def save(self, data):
        self.operation(lambda fp: json.dump(fp=fp, obj=data), mode='w')


class Folder(PathSubclass):

    def is_file(self):
        return False

    def create(self):
        os.makedirs(str(self), exist_ok=True)

    def copy_new_file(self, src: File, fname=None):
        fname = fname if fname else os.path.split(str(src))[1]
        shutil.copy(str(src), os.path.join(str(self), fname))

    def delete(self):
        shutil.rmtree(str(self))


class MetaWorkspace(ABC):

    @property
    @abstractmethod
    def base_dir(self):
        pass

    @abstractmethod
    def create_all(self):
        pass

    @abstractmethod
    def clear(self):
        pass

    @abstractmethod
    def setup_action(self) -> Action:
        pass


class Workspace(MetaWorkspace):

    def __init__(self, base_dir, flatten=False):
        self._base_dir = Folder(base_dir)
        self._flatten = flatten

    @property
    def base_dir(self):
        return Folder(self._base_dir)

    def _join_path(self, *args, flatten=None):
        flatten = flatten if flatten is not None else self.is_flat()
        as_strings = [str(arg) for arg in args]

        if flatten:
            return File(self.base_dir, as_strings[-1]) if len(args) > 0 and args[-1].is_file() else Folder(
                self.base_dir)
        return File(self.base_dir, *as_strings) if len(args) > 0 and args[-1].is_file() else Folder(
            os.path.join(str(self._base_dir), *as_strings))

    def create_all(self):
        self.base_dir.create()

    def clear(self):
        if not self.is_flat():
            self.base_dir.delete()
        else:
            logging.warning('Deleting flattened base repos has been disabled')

    def is_flat(self):
        return self._flatten

    def setup_action(self) -> Action:
        return ActionThread()


class TrainWorkspace(Workspace):

    def __init__(self, base_dir, flatten=False):
        super().__init__(base_dir, flatten=flatten)

    @property
    def t2t_problems_dir(self):
        return Folder(self._join_path(Folder('t2tproblems'), flatten=False))

    @property
    def t2t_problems_workspace(self):
        return T2TProblemsWorkspace(self.t2t_problems_dir, flatten=False)

    @property
    def tensor2tensor_dir(self):
        return Folder(self._join_path(Folder('tensor2tensor'), flatten=False))

    @property
    def tensor2tensor_workspace(self):
        return T2TWorkspace(self.tensor2tensor_dir, flatten=False)

    @property
    def model_dir(self):
        return Folder(self._join_path(Folder('gym')))

    @property
    def model_workspace(self):
        return ModelWorkspace(self.model_dir)

    @property
    def log_dir(self):
        return self._join_path(Folder('logs'))

    @property
    def train_stdout_log(self):
        return self._join_path(self.log_dir, File('train.stdout'))

    @property
    def train_stderr_log(self):
        return self._join_path(self.log_dir, File('train.stderr'))

    @property
    def scripts_dir(self):
        return self._join_path(Folder('scripts'))

    @property
    def train_script(self):
        return self._join_path(self.scripts_dir, File('train.sh'))

    def create_all(self):
        super().create_all()
        self.t2t_problems_dir.create()
        self.tensor2tensor_dir.create()
        self.model_dir.create()
        self.log_dir.create()
        self.scripts_dir.create()

    def clear(self):
        if not self.is_flat():
            self.t2t_problems_dir.delete()
            self.tensor2tensor_dir.delete()
            self.model_dir.delete()
            self.log_dir.delete()
            self.scripts_dir.delete()
        else:
            logging.warning('Not going to delete folders in flattened base repos')

    def setup_action(self) -> Action:
        action = ActionThread()

        action.add_action(lambda _: CreateFolderAction(str(self.base_dir)))

        create_folder_action = CreateFolderAction(str(self.log_dir))
        action.add_action(lambda _: create_folder_action)

        # Need to setup t2tproblems
        t2t_problems_action = GitSetupAction(str(self.t2t_problems_dir),
                                             git_repo='git@bitbucket.org:nmt_tradooit/t2tproblems.git')
        action.add_action(lambda _: t2t_problems_action)

        # Need to setup tensor2tensor
        t2t_codebase_action = GitSetupAction(str(self.tensor2tensor_dir),
                                             git_repo='git@github.com:smcdufff/t2t_constraint.git',
                                             branch='training')
        action.add_action(lambda _: t2t_codebase_action)

        setup_virtualenv = VirtualEnvAction(str(self.tensor2tensor_dir), install_requirements=True)
        action.add_action(lambda _: setup_virtualenv)

        return action


class T2TWorkspace(Workspace):

    @property
    def bin_path(self):
        return self._join_path(self.code_path, Folder('bin'))

    @property
    def code_path(self):
        return self._join_path(Folder('tensor2tensor'))

    @property
    def training_script(self):
        return self._join_path(self.bin_path, File('t2t-trainer'))

    @property
    def translate_script(self):
        return self._join_path(self.bin_path, File('t2t-translate-all'))

    @property
    def export_script(self):
        return self._join_path(self.bin_path, File('t2t-exporter'))

    @property
    def decode_script(self):
        return self._join_path(self.bin_path, File('t2t-decoder'))

    def create_all(self):
        super().create_all()
        self.code_path.create()

    def clear(self):
        if not self.is_flat():
            self.code_path.delete()
        else:
            logging.warning('Not going to delete folders in flattened base repos')
            self.training_script.delete()
            self.translate_script.delete()
            self.decode_script.delete()
            self.export_script.delete()

    def setup_action(self) -> Action:
        return ActionThread()


class T2TProblemsWorkspace(Workspace):

    @property
    def t2t_problems_path(self):
        return self._join_path(Folder('t2t_problems'))

    def clear(self):
        if not self.is_flat():
            self.t2t_problems_path.delete()
        else:
            logging.warning('Not going to delete folders in flattened base repos')

    def create_all(self):
        super().create_all()
        self.t2t_problems_path.create()

    def setup_action(self) -> Action:
        return ActionThread()


class ModelWorkspace(Workspace):

    @property
    def checkpoint_file(self):
        return self._join_path(File('checkpoint'))

    def clear(self):
        self.checkpoint_file.delete()

    def setup_action(self) -> Action:
        return ActionThread()


class TestWorkspace(TrainWorkspace):

    @property
    def test_data_dir(self):
        return self._join_path(Folder('test_data'))

    @property
    def translate_script(self):
        return self._join_path(self.scripts_dir, File('translate.sh'))

    def test_file(self, lang):
        return self._join_path(self.test_data_dir, File('source_test.{}'.format(str(lang))))

    def test_file_tokenized(self, lang):
        return self._join_path(self.test_data_dir, File('source_test.tok.{}'.format(str(lang))))

    @property
    def test_stdout_log(self):
        return self._join_path(self.log_dir, File('test.stdout'))

    @property
    def test_stderr_log(self):
        return self._join_path(self.log_dir, File('test.stderr'))

    def create_all(self):
        super().create_all()
        self.test_data_dir.create()

    def clear(self):
        if not self.is_flat():
            self.test_data_dir.delete()
        else:
            logging.warning('Not going to delete folders in flattened base repos')
            self.translate_script.delete()

    def setup_action(self) -> Action:
        return ActionThread()


class ModelSelectionWorkspace(TestWorkspace):

    @property
    def top_model_save_dir(self):
        return Folder(self._join_path(Folder('top_models'), flatten=False))

    @property
    def top_model_save_index_path(self) -> JSONFile:
        return JSONFile(self._join_path(self.top_model_save_dir, JSONFile('index.json'), flatten=False))

    @property
    def top_model_state_path(self) -> JSONFile:
        return JSONFile(self._join_path(self.top_model_save_dir, JSONFile('state.json'), flatten=False))

    def create_all(self):
        super().create_all()
        self.top_model_save_dir.create()

    def clear(self):
        if not self.is_flat():
            self.top_model_save_dir.delete()
        else:
            logging.warning('Not going to delete folders in flattened base repos')
            self.top_model_state_path.delete()
            self.top_model_save_index_path.delete()

    def setup_action(self) -> Action:
        return ActionThread()


class ExporterWorkspace(TrainWorkspace):

    @property
    def export_script(self):
        return self._join_path(self.scripts_dir, File('export.sh'))

    @property
    def test_stdout_log(self):
        return self._join_path(self.log_dir, File('export.stdout'))

    @property
    def test_stderr_log(self):
        return self._join_path(self.log_dir, File('export.stderr'))

    def setup_action(self) -> Action:
        return ActionThread()
