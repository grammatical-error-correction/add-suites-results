import datetime
import glob
import logging
import os


class MajorMinorRevisionVersioning:

    def __init__(self, major, minor, revision):
        self._major, self._minor, self._revision = major, minor, revision

    @staticmethod
    def from_string(unparsed):
        major, minor, revision = os.path.split(unparsed)[1].split('.')
        return MajorMinorRevisionVersioning(int(major), int(minor), int(revision))

    @property
    def major(self):
        return self._major

    @property
    def minor(self):
        return self._minor

    @property
    def revision(self):
        return self._revision

    def __eq__(self, other):
        return other.major == self.major and other.minor == self.minor and other.revision == self._revision

    def __ne__(self, other):
        return other.major != self.major or other.minor != self.minor or other.revision != self._revision

    def __ge__(self, other):
        if self.major > other.major:
            return True
        if self.major < other.major:
            return False
        if self.minor > other.minor:
            return True
        if self.minor < other.minor:
            return False

        return self.revision >= other.revision

    def __gt__(self, other):
        if self.major > other.major:
            return True
        if self.major < other.major:
            return False
        if self.minor > other.minor:
            return True
        if self.minor < other.minor:
            return False

        return self.revision > other.revision

    def __le__(self, other):
        if self.major < other.major:
            return True
        if self.major > other.major:
            return False
        if self.minor < other.minor:
            return True
        if self.minor > other.minor:
            return False

        return self.revision <= other.revision

    def __lt__(self, other):
        if self.major < other.major:
            return True
        if self.major > other.major:
            return False
        if self.minor < other.minor:
            return True
        if self.minor > other.minor:
            return False

        return self.revision < other.revision

    def __repr__(self):
        return str(self)

    def __str__(self):
        return '{}.{}.{}'.format(self.major, self.minor, self.revision)


class DateVersioning:
    _FORMATS = ['%Y-%m-%d', '%Y%m%d']

    def __init__(self, date, date_format=None):
        self._date = date
        self._format = date_format if date_format else DateVersioning._FORMATS[0]

    @property
    def date(self):
        return self._date

    def __repr__(self):
        return str(self)

    def __str__(self):
        return self._date.strftime(self._format)

    def __eq__(self, other):
        return self.date == other.date

    def __ne__(self, other):
        return self.date != other.date

    def __ge__(self, other):
        return self.date >= other.date

    def __gt__(self, other):
        return self.date > other.date

    def __le__(self, other):
        return self.date <= other.date

    def __lt__(self, other):
        return self.date < other.date

    @staticmethod
    def from_string(string):
        string = os.path.split(string)[1]
        for date_format in DateVersioning._FORMATS:
            try:
                return DateVersioning(datetime.datetime.strptime(string, date_format), date_format)
            except ValueError:
                pass
        logging.error("Invalid date time %s", string)
        return None


class NumberVersioning:

    def __init__(self, number: int):
        self.number = number

    def __repr__(self):
        return str(self)

    def __str__(self):
        return str(self.number)

    def __eq__(self, other):
        return self.number == other.number

    def __ne__(self, other):
        return self.number != other.number

    def __ge__(self, other):
        return self.number >= other.number

    def __gt__(self, other):
        return self.number > other.number

    def __le__(self, other):
        return self.number <= other.number

    def __lt__(self, other):
        return self.number < other.number

    @staticmethod
    def from_string(string):
        string = os.path.split(string)[1]
        try:
            return NumberVersioning(int(string))
        except ValueError:
            return None


def get_max_version(path, versionning):
    versions = list()
    for candidate_path in glob.glob(os.path.join(path, '*')):
        try:
            versions.append(versionning.from_string(candidate_path))
        except ValueError:
            pass
    return str(max(filter(None, versions))) if versions else None
