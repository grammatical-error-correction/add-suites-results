import argparse
import pprint
from abc import ABC, abstractmethod
from typing import Optional

from nmtcoreutils.models.dto import ApiDTO


class WebCommand(ABC):

    @abstractmethod
    def execute(self, url: str) -> Optional[ApiDTO]:
        pass


def get_args(description: str):
    parser = argparse.ArgumentParser(description)
    parser.add_argument("command_terms", help="Operation to perform", nargs="+")
    parser.add_argument('--host', help='Host name', required=False, default='localhost', dest='host')
    parser.add_argument('-p', '--port', help='Port', required=False, type=int, default=50000, dest='port')
    args = parser.parse_args()
    return args.command_terms, args.host, args.port


def get_synonym_root(term: str, synonyms: dict) -> str:
    if term in synonyms:
        return term
    for root, syns in synonyms.items():
        if term in syns:
            return root
    return term


def get_command(command_terms, tree, synonyms) -> Optional[WebCommand]:
    root = tree
    for i, term in enumerate(command_terms):
        if not isinstance(root, dict):
            return root(*command_terms[i:])

        root_term = get_synonym_root(term, synonyms=synonyms)
        if root_term not in root:
            print("Unacceptable command: {}".format(term))
            print("Acceptable commands are: {}".format(", ".join(root.keys())))
            return None

        root = root.get(root_term)

    if isinstance(root, dict):
        print("Incomplete command, please pick from the following: {}".format(", ".join(root.keys())))
        return None
    
    return root()


def run_web_client(description: str, commands: dict, synonyms: dict):
    command_terms, hostname, port = get_args(description)
    url = "http://{}:{}".format(hostname, port)
    command = get_command(command_terms, tree=commands, synonyms=synonyms)

    if not command:
        print("Exiting")
        return 1

    data = command.execute(url)
    if data is None:
        print("Command failed")
        return 2
    pprint.pprint(data)
    return 0
