from abc import abstractmethod, ABC


class RemoteCommandExecutor(ABC):

    @property
    @abstractmethod
    def host(self) -> str:
        pass

    @property
    @abstractmethod
    def username(self) -> str:
        pass

    @abstractmethod
    def run(self, cmd, **kwargs):
        pass
