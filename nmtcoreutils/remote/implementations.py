import subprocess

from nmtcoreutils.remote.executor import RemoteCommandExecutor


class SSHRemoteCommandExecutor(RemoteCommandExecutor):

    def __init__(self, username, host):
        self._host = host
        self._username = username

    @property
    def host(self):
        return self._host

    @property
    def username(self):
        return self._username

    def run(self, cmd, **kwargs):
        timeout = kwargs.get('timeout', None)
        no_output = kwargs.get('no_output', False)
        if no_output:
            return subprocess.call(
                'ssh {}@{} "{}"'.format(self._username, self._host, cmd), shell=True, timeout=timeout
            )
        return subprocess.check_output(
            'ssh {}@{} "{}"'.format(self._username, self._host, cmd), shell=True, timeout=timeout
        ).decode('utf-8')
