# MT Compare: Tutorial for Speller

Thomas Monnier
@Reverso, 25/02/2022

# 0. Requirements

- MT Compare account (contact Alexander Khoninev to get one)
- BitBucket account (contact Benjamin Boigienman to get one)
- Python3
- (Optional) Git

# 1. Create the Test Suite

- Log in to [MT Compare](http://mtcompare.reverso.net/)
- Go to the [Test Suites](http://mtcompare.reverso.net/TS)
- Click on the green ➕
	- Direction: 
		- “French” → “French” (French Spell checker)
		- “English” → “English” (English Spell checker)
	- Test Suite Type: “Speller”
	- Name
	- Friendly Name
	- Tags
	- Linked TS

⚡ Feature underlined is required.

# 2. Clone the repository

You have to clone the [BitBucket Repository](https://bitbucket.org/grammatical-error-correction/add-suites-results/src/master/). Be aware that some changes can be made at any time. Remember to check the latest version and pull if there is one.

# 3. Pre-requisites

⚡ You need to have a config.ini file in your directory, containing these lines:

```
[MTCompare]
host=mtcompare.reverso.net
port=80
username=USERNAME
password=USERPASSWORD
```
 
**USERNAME** and **USERPASSWORD** are your MTCompare’s IDs.
 
⚡You need to install all Python packages that are necessary to be able to run the models. To do so, open a terminal in the directory of the repository, and run:

`pip3 install -r requirements.txt`

❗If you see an error message (in red) during the installation, you just have to re-run the command line (important for ProWritingAid’s model).

# 4. Upload the CSV file 

Open a terminal and run:

```
python3 add_to_mtcompare_suite.py --test_id TEST_ID		#test suite ID *
					     		--path PATH				#path of the CSV set to upload
					    		--source SOURCE			#name of the source column in the CSV set
					    		--target TARGET			#name of the target column in the CSV set
					    		--category CATEGORY  	#mistake category ** targeted in the CSV set
```

Example: 
`python3 add_to_mtcompare_suite.py --test_id *********** --path file.csv`

*: The test suite ID can be found on the link of a test suite (highlighted in bold):
http://mtcompare.reverso.net/TS/Home/TSSourcesIndex/**dcd16980-079b-4d49-bf96-ea8617b7302b**?Source=&Reference=

**: The existing categories on MTCompare are listed in the [AdminTools](http://mtcompare.reverso.net/TS/Home/TSCategoriesIndex )

⚡ The arguments underlined are required.

Wait until all the file has been uploaded:
150 ~ 200 sentences (Original and Expected) - 1 minute

# 5. (Optional) New model or new version

In order to run a new model on MT Compare’s Test Suites:

- In your repository, go to models/ and create a directory named as your model.
- Inside this directory, put all the files that are necessary for your model to be run
- Create a file predict_for_mtcompare.py which contains the function predict_for_mtcompare() which takes as inputs the source sentences in a Python list and returns the corrections in a Python list

# 6. Run a model

- If you want to run Ginger, go to the Test Suite on MT Compare, and click on  next to , then select one of the three Ginger engines proposed, add a tag and optionally a version and a description, and wait until the process is over.

- If you want to run another model, open a terminal and run:

```
python3 add_to_mtcompare_results.py --test_id TEST_ID		#test suite ID
                                    --model MODEL           #model path *
					        		--name NAME		        #model name
                                    --version VERSION       #model version
```
 
Example: 
`python3 add_to_mtcompare_results.py --test_id ******** --model models.Gramformer.predict_for_mtcompare --name Gramformer`

*: The model path needs to be written as a package. If your predict_for_mtcompare.py is in Models/MODEL/, then test_id = models.MODEL.predict_for_mtcompare

⚡ The arguments underlined are required.

🧠 Ginger is already instanced on MT Compare. You just need to click on “Run test suite” and select the version of Ginger that you want.

Wait until all the processed sentences have been uploaded.

# 7. Select results

- Go to the Test Suite on MT Compare and click on + Select Results
- Select the results of the model (+version) you want to analyze and click on OK 

You can recalculate scores or delete the results.

# 8. Scores

Globally, when selecting a model, you will find:

- Number of fails / Number of total inputs
- PI score
- GLEU
- F-score for Correction: ⚡The β chosen for now is 1. It needs to be changed to 0.5.
- F-score for Detection: ⚡The β chosen for now is 1. It needs to be changed to 0.5.
- For every sentence, a weighted score " X / Y " is computed:
- X: System of grades (1: Normal, then Bad, Poor, etc) 
- Y: GLEU
 
For every sentence, when you click on info:

- TP: True Positives / incorrect tokens corrected
- TN: True Negatives / well-written tokens not corrected
- FP: False Positives / well-written tokens corrected
- FN: False Negatives / incorrect tokens not corrected
- FPN: Intersection of FPs and FNs (only for correction). It is increased when a source, target, and reference words are all different, meaning that a position was detected correctly, but the correction itself was wrong
- Precision: TP / (TP + FP)
- Recall: TP / (TP+FN)
- F-score: ⚡The β chosen for now is 1. It needs to be changed to 0.5.
- ACC: Accuracy